# Mockito Examples

## Getting started

## Prerequisite

JDK 8 and Git must be installed in order to run the project locally.

## Cloning the repository and running tests

Clone the repository:

git clone [https://github.com/redmond007/mockito-intro.git](https://github.com/redmond007/mockito-intro.git)

Navigate to newly created folder and run:

`gradlew test`

**References**

[Mockito](site.mockito.org/mockito/docs/current/org/mockito/Mockito.html)

[JUnit](http://junit.sourceforge.net/javadoc/)