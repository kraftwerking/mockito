package com.kraftwerking.mockito;


import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class MockitoIntroApplicationTests {

    @Mock
    TestDAO testDAO;

    @InjectMocks
    TestService testService;

    //create spy w annotations
    @Spy
    TestDAO spyTestDAO2 = new TestDAO();

    @Captor
    ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);


    @Test
    public void testUpdateOperation() throws Exception {
        //configure return behavior for mock
        when(testDAO.updateOperation())
                .thenReturn("test query result");
        String testResult = testService.runUpdateOperation();
        assertEquals(testResult, "test query result");
    }

    @Test(expected = IllegalStateException.class)
    public void testUpdateOperationExpectException() throws Exception {
        //configure return behavior for mock to return an exception on method call
        when(testDAO.updateOperation())
                .thenThrow(IllegalStateException.class);
        String testResult = testService.runUpdateOperation();

    }

    @Test(expected = IllegalStateException.class)
    public void testUpdateOperationExpectExceptionMultipleCalls() throws Exception {
        //configure return behavior for mock to return an exception on multiple method call
        when(testDAO.updateOperation())
                .thenReturn("test query result")
                .thenThrow(IllegalStateException.class);
        String testResult = testService.runUpdateOperation();
        assertEquals(testResult, "test query result");
        testResult = testService.runUpdateOperation();

    }

    @Test(expected = NullPointerException.class)
    public void testUpdateVoidOperation() throws Exception {
        //configure return behavior for mock for void return type
        doThrow(NullPointerException.class).when(testDAO).updateVoidOperation();
        testService.runUpdateVoidOperation();

    }

    @Test
    public void testRealUpdateOperation() throws Exception {
        //configure method to call real underlying method on the mock
        when(testDAO.updateOperation()).thenCallRealMethod();
        String testResult = testService.runUpdateOperation();
        assertEquals(testResult, "query result");

    }


    @Test
    public void testAdd() {
        //configure return behavior for mock to return a different value
        when(testDAO.add(1, 2)).thenReturn(4);
        //test the behavior
        assertEquals(testDAO.add(1, 2), 4, 0);
        //verify the behavior occurred
        verify(testDAO).add(1, 2);
    }

    @Test
    public void testAddNot() {
        //configure return behavior for mock to return a different value
        when(testDAO.add(1, 2)).thenReturn(4);
        //verify the behavior has not occurred
        verify(testDAO, never()).add(1, 2);
    }

    @Test
    public void testAddZeroInteractions() {
        //configure return behavior for mock to return a different value
        when(testDAO.add(1, 2)).thenReturn(4);
        //verify zero interactions with the mocked class has occurred
        verifyZeroInteractions(testDAO);
    }

    @Test
    public void testAddOrderInteractions() {
        //configure return behavior for mock to return a different value
        when(testDAO.add(1, 2)).thenReturn(4);
        when(testDAO.add(1, 3)).thenReturn(5);
        //test the behavior
        assertEquals(testDAO.add(1, 2), 4, 0);
        assertEquals(testDAO.add(1, 3), 5, 0);

        InOrder mockInOrder = Mockito.inOrder(testDAO);

        //verify the behavior occurred in order
        mockInOrder.verify(testDAO).add(1, 2);
        mockInOrder.verify(testDAO).add(1, 3);
    }

    @Test
    public void testAddAnyArgs() {
        //configure return behavior for mock to return a different value
        when(testDAO.add(1, 2)).thenReturn(4);
        //test the behavior
        assertEquals(testDAO.add(1, 2), 4, 0);
        //verify the behavior occurred with any argument of a specific type
        verify(testDAO).add(anyInt(), anyInt());
    }

    @Test
    public void testAddTimes() {
        //configure return behavior for mock to return a different value
        when(testDAO.add(1, 2)).thenReturn(4);
        //test the behavior
        assertEquals(testDAO.add(1, 2), 4, 0);
        assertEquals(testDAO.add(1, 2), 4, 0);
        //verify the behavior occurred a number of times
        verify(testDAO, times(2)).add(1, 2);
    }

    @Test
    public void testAddSpy() {
        //create spy
        TestDAO testDAO = new TestDAO();
        TestDAO spyTestDAO = spy(testDAO);

        //call normal methods on an object
        spyTestDAO.setId("this id");
        verify(spyTestDAO).setId("this id");
        assertEquals("this id", spyTestDAO.getId());
    }

    @Test
    public void testAddSpyAnnotations() {
        //call normal methods on an object
        spyTestDAO2.setId("this id");
        verify(spyTestDAO2).setId("this id");
        assertEquals("this id", spyTestDAO2.getId());
    }

    @Test
    public void testAddSpyAnnotationsDoReturn() {
        //call normal methods on an object
        spyTestDAO2.setId("this id");
        verify(spyTestDAO2).setId("this id");
        assertEquals("this id", spyTestDAO2.getId());

        //configure behavior of spy object with doReturn
        doReturn("that id").when(spyTestDAO2).getId();
        assertEquals("that id", spyTestDAO2.getId());
    }

    @Test
    public void testUpdateOperationCaptor() throws Exception {
        testDAO.setId("this id");
        verify(testDAO).setId((String) argumentCaptor.capture());
        assertEquals("this id", argumentCaptor.getValue());
    }


}
