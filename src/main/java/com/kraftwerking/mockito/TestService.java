package com.kraftwerking.mockito;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service(value = "testService")
public class TestService {

    @Autowired
    @Qualifier("testDAO")
    TestDAO testDAO;

    public String runUpdateOperation() {
        String result = testDAO.updateOperation();
        return result;
    }

    public void runUpdateVoidOperation() {
        testDAO.updateVoidOperation();
    }

    public int runAdd(int a, int b) {
        int result = testDAO.add(a, b);
        return result;
    }
}