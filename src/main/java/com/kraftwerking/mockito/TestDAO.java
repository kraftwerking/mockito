package com.kraftwerking.mockito;

import org.springframework.stereotype.Component;

@Component(value = "testDAO")
public class TestDAO {

    public String id;

     //would perform some hibernate query
    public String updateOperation(){
        return "query result";
    }

     //would perform some hibernate query
    public void updateVoidOperation(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int add(int a, int b){
        return a + b;
    }

}
